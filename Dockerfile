FROM cm2network/steamcmd:root@sha256:7adb750f5766ba404480afcb6735d283e8dee226c0d47a27dceecc1d1ed2d106

RUN apt-get update && apt-get install -y \
  libxi6 \
  xvfb \
  && rm -rf /var/lib/apt/lists/*

# TODO: steamcmd install fails without set -x below. WHY?
RUN set -x mkdir -p /home/steam/core-keeper-server
RUN /home/steam/steamcmd/steamcmd.sh +force_install_dir /home/steam/core-keeper-server +login anonymous +app_update 1007 +app_update 1963720 +quit

WORKDIR /home/steam/core-keeper-server

ENV WORLD_NAME="Core Keeper Server"
ENV WORLD_SEED="0"
ENV GAME_ID=""
ENV MAX_PLAYERS=100
ENV WORLD_MODE="0"
ENV DIRECT_CONNECT="0"
ENV IP_BIND_ADDRESS="0.0.0.0"

# Link important data to a well-known dir
RUN ln -sf "/root/.config/unity3d/Pugstorm/Core Keeper/DedicatedServer/" "/data"

COPY --chmod=775 entrypoint.sh ./

ENTRYPOINT ["./entrypoint.sh"]

EXPOSE 27015
