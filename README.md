# Core Keeper Server

Core Keeper Server is an OCI container image for hosting a dedicated server for the game [Core Keeper](https://store.steampowered.com/app/1621690/Core_Keeper/).

> :exclamation: **Deprecation Notice**: Beginning **2022-09-06**, the GitLab Container Repository for this image is deprecated. No further images will be published there and they will be gradually removed. The images are simply too large and use up too much of my GitLab storage. The Docker Hub and Quay repositories are still supported (as long as their storage remains unlimited). I recommend switching to the Quay repository, regardless.

## Features

- Automatically rebuilt using [Steamtrigger](https://gitlab.com/tedtramonte/steamtrigger) when the server is updated
  - This means the server can be launched as a specific version, in case players want to play a specific version
  - Can still be automatically updated using [Watchtower](https://github.com/containrrr/watchtower/) or similar tools
- Server logs are in `stdout`
  - This means you can see connect/disconnect events and more in the logs of the container

## Requirements

- An OCI compliant container engine like Docker or Podman

## Usage

```bash
# Basic usage with Steam relay
docker run -d -v /custom/path/to/my/server-data:/data --restart unless-stopped quay.io/tedtramonte/core-keeper-server:latest
# Basic usage with direct connection
docker run -d -v /custom/path/to/my/server-data:/data -e DIRECT_CONNECT="1" -p 27015:27015 --restart unless-stopped quay.io/tedtramonte/core-keeper-server:latest

# To easily check the Game ID to connect with
# where $CONTAINER_NAME_OR_ID is the running container's name or unique ID
docker exec $CONTAINER_NAME_OR_ID cat GameID.txt

# Customize settings
docker run -d -e WORLD_NAME="My Server" -e WORLD_SEED="12345" -e GAME_ID="AGameId" -e MAX_PLAYERS="8" -v /custom/path/to/my/server-data:/data --restart unless-stopped quay.io/tedtramonte/core-keeper-server:latest
# Run with a non-standard port, if running multiple servers on the same host
docker run -d -e DIRECT_CONNECT="1" -p 9090:27015 -v /custom/path/to/my/server-data:/data --restart unless-stopped quay.io/tedtramonte/core-keeper-server:latest

# Windows
docker run -d -v "C:\Custom\Path\To\My\Server Data":/data --restart unless-stopped quay.io/tedtramonte/core-keeper-server:latest

# Run a specific build
docker run -d -e SERVER_NAME="My Server" -v /custom/path/to/my/server-data:/data --restart unless-stopped quay.io/tedtramonte/core-keeper-server:8718471
```

Or, if you prefer `docker-compose`:

```yaml
services:
  core_keeper_server:
    image: quay.io/tedtramonte/core-keeper-server:latest
    restart: unless-stopped
    # Only needed if using DIRECT_CONNECT=1
    # ports:
    #   - "9090:27015"

    # Adjust configuration as desired
    # environment:
    #   WORLD_NAME: "My Server"
    #   WORLD_SEED: "12345"
    #   GAME_ID: "AGameId"

    volumes:
      # Bind mount, to access the files directly on the host
      - /custom/path/to/my/server-data:/data
      # Ephemeral volume, to persist data without caring about access from the host
      # - core_keeper_server_data:/data
# Uncomment if using ephemeral volume
# volumes:
#   core_keeper_server_data:
```

## Configuration

### Environment Variables

- `WORLD_NAME` - The name to use for the server. (default: "Core Keeper Server")
- `WORLD_SEED` - The seed to use for a new world. Set to 0 to generate random seed. (default: 0)
  - Must be numeric (0-9) or will fall back to generating a random seed.
- `GAME_ID` - Game ID to use for the server. Empty or not valid means a new ID will be generated at start. (default: "")
  - Need to be at least 23 characters and alphanumeric, excluding Y,y,x,0,O.
  - If `PORT` is set, this is ignored. The server will still log it as if it's useful, but it should be ignored.
- `MAX_PLAYERS` - Maximum number of players that will be allowed to connect to server. (default: 100)
- `WORLD_MODE` - Whether to use normal (0) or hard (1) mode for world. (default: 0)
- `DIRECT_CONNECT` - Whether to use Steam relay network (0) or a direct IP connection (1). (default: 0)
  - If set to "1" the clients will connect to the server directly by IP:PORT and the port needs to be open.
- `IP_BIND_ADDRESS` - Only used if `DIRECT_CONNECT` is set to "1". Sets the address that the server will bind to. (default: 0.0.0.0)
  - You shouldn't need to adjust this, but it's an option just in case.

### Ports

- `27015` - Primary game port.

### Volumes

In order to persist your server's data between restarts, the `/data` directory must be kept in a volume. A bind mount is recommended to easily adjust the files yourself, but if you don't need to transfer files into the container an ephemeral volume will work as well.

```bash
# Bind mount: Admins.json, ServerConfig.json, and worlds dirs will be accessible on this path
docker run -d -v /custom/path/to/my/server-data:/data --restart unless-stopped quay.io/tedtramonte/core-keeper-server:latest

# Ephemeral volume: data will persist but be inaccessible from outside the container
docker run -d -v my-core-keeper-server-data:/data --restart unless-stopped quay.io/tedtramonte/core-keeper-server:latest
```

## Contributing

Merge requests are welcome after opening an issue first.
